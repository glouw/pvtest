from gnuradio import analog
from gnuradio import blocks
from gnuradio import uhd
from gnuradio import gr

import crimson
import threading
import sys
import time
import numpy

# Threads make exiting upon a failure complicated. This boolean
# will be returned to the command line once the test is complete.
failure = False

def fail(message):
    sys.stderr.write(message + "\n")

    global failure
    failure = True

def dump(vsnk, sample_count, channels):
    """
    Prints a vsnk in channel column layout in IQ format for all channels.
    """
    for sample in xrange(sample_count):
        for channel in channels:
            datum = vsnk[channel].data()[sample]
            sys.stdout.write("%10.5f %10.5f\t" % (datum.real, datum.imag))
        sys.stdout.write("\n")

    sys.stdout.write("\n")

def absolute_area(data):
    """
    Takes and IQ wave.
    Absolutes then integrates.
    The complex module of the integral is return.
    """
    return abs(numpy.trapz(numpy.absolute(data)))

def get_fullness(data, sample_count, total_sample_count):
    """
    Here, 'x' denotes a wave, and '-' denotes no data (zero DC).

    { ------- lhs ------ } { ------- rhs ------ }
    t[0]                                        t[1]
    ------------------xxxxxxxxxxxxxxxxxxxxxxxxxxx------------------xxxxx ...
    |                     |                     |
    0                     sample_count / 2      sample_count

    A wave is 100% full when TX and RX start at the same time:
        -> eg. 512 samples of TX are sampled correctly for 512 samples of RX.

    t[n] indicates each stacked RX command in <data>.

    The entirey of <data> with all stacked RX commands are absolute integrated
    to get the actual wave fullness.

    The last stacked RX command is partitioned into its left hand side (lhs)
    and right hand side (rhs).
    The greater of the two is absolute integrated and multiplied to get the
    best case estimated absolute area.
    NOTE: The last stacked RX command is used as the first stacked RX command
    exibits power on noise.

    This function returns 1.0 if the wave is full (eg. TX and RX start at the same time).
    """
    assert total_sample_count >= 2 * sample_count # Needs at least 2 stacked RX commands.

    a = total_sample_count - sample_count
    b = total_sample_count - sample_count / 2
    c = total_sample_count
    lhs = absolute_area(data[a : b])
    rhs = absolute_area(data[b : c])

    estimated = (total_sample_count / sample_count) * 2 * (lhs if lhs > rhs else rhs)

    actual = absolute_area(data)

    return actual / estimated

def fullness_test(vsnk, sample_count, total_sample_count):
    """
    Tests a vsnk across all channels to see of each channel's wave fullness
    is greater than some threshold amount.
    """
    for channel, v in enumerate(vsnk):
        fullness = get_fullness(v.data(), sample_count, total_sample_count)
        print fullness
        threshold = 0.6
        if fullness < threshold:
            fail("error: fullness test: channel %d: fullness was %f which "
                 "is below threshold %f" % (channel, fullness, threshold))

def tx_run(csnk, channels, sample_count, start_time_specs, sample_rate):
    """                                       +-----------+
    +---------+   +---------+   +---------+   |           |
    | sigs[0] |-->| heds[0] |-->| c2ss[0] |-->|ch[0]      |
    +---------+   +---------+   +---------+   |           |
    +---------+   +---------+   +---------+   |           |
    | sigs[1] |-->| heds[1] |-->| c2ss[1] |-->|ch[1]      |
    +---------+   +---------+   +---------+   |           |
                                              |           |
    +---------+   +---------+   +---------+   |           |
    | sigs[N] |-->| heds[N] |-->| c2ss[N] |-->|ch[N]      |
    +---------+   +---------+   +---------+   |      csnk |
                                              +-----------+
    """

    sigs = [analog.sig_source_c(
        sample_rate, analog.GR_SIN_WAVE, 1.0e6, 2.0e4, 0.0)
        for ch in channels]

    heds = [blocks.head(gr.sizeof_gr_complex, sample_count)
        for ch in channels]

    c2ss = [blocks.complex_to_interleaved_short(True)
        for ch in channels]

    flowgraph = gr.top_block()
    for ch in channels:
        flowgraph.connect(sigs[ch], heds[ch])
        flowgraph.connect(heds[ch], c2ss[ch])
        flowgraph.connect(c2ss[ch], (csnk, ch))

    for start_time_spec in start_time_specs:
        csnk.set_start_time(start_time_spec)
        flowgraph.run()
        for hed in heds:
            hed.reset()

def rx_run(csrc, channels, sample_count, start_time_specs, sample_rate):
    """
    +-----------+
    |           |   +---------+
    |      ch[0]|-->| vsnk[0] |
    |           |   +---------+
    |           |   +---------+
    |      ch[1]|-->| vsnk[1] |
    |           |   +---------+
    |           |
    |           |   +---------+
    |      ch[N]|-->| vsnk[N] |
    | csrc      |   +---------+
    +-----------+
    """

    vsnk = [blocks.vector_sink_c() for channel in channels]

    flowgraph = gr.top_block()
    for channel in channels:
        flowgraph.connect((csrc, channel), vsnk[channel])

    # The flowgraph must be started before stream commands are sent.
    flowgraph.start()

    for start_time_spec in start_time_specs:
        cmd = uhd.stream_cmd_t(uhd.stream_cmd_t.STREAM_MODE_NUM_SAMPS_AND_DONE)
        cmd.num_samps = sample_count
        cmd.stream_now = False
        cmd.time_spec = start_time_spec
        csrc.issue_stream_cmd(cmd)

    total_sample_count = len(start_time_specs) * sample_count

    # Wait for completion.
    while len(vsnk[0].data()) < total_sample_count:
        time.sleep(0.1)

    flowgraph.stop()
    flowgraph.wait()

    dump(vsnk, sample_count, channels)

    fullness_test(vsnk, sample_count, total_sample_count)

def test_tx_rx_loopback():

    channels = range(2)

    csnk = crimson.make_snk_s(channels)
    csrc = crimson.make_src_c(channels)

    # All of these can be iterated.
    sample_rate = 20e6
    sample_count = 256
    rx_gain = 30.0
    tx_gain = 30.0

    for iteration in range(1):

        for center_freq in [15e6, 4e9]:

            crimson.calibrate(csnk, channels, sample_rate, center_freq, tx_gain)
            crimson.calibrate(csrc, channels, sample_rate, center_freq, rx_gain)

            now = csrc.get_time_now()

            start_time_specs = [
                now + uhd.time_spec(1.5),
                now + uhd.time_spec(3.0),
                now + uhd.time_spec(4.5),
                now + uhd.time_spec(6.0),
                ]

            # More than one start time spec has to be used as stacked RX and TX commands are a testing requirement.
            # Also, fullness tests use the last RX stacked command data for determining wave fullness since the first
            # stacked RX command exibits some startup noise when the receiver turns on.
            assert len(start_time_specs) > 1

            threads = [
                threading.Thread(target = tx_run, args = (csnk, channels, sample_count, start_time_specs, sample_rate)),
                threading.Thread(target = rx_run, args = (csrc, channels, sample_count, start_time_specs, sample_rate)),
                ]

            for thread in threads:
                thread.start()

            for thread in threads:
                thread.join()

    quit(1 if failure else 0)

if __name__ == '__main__':
    test_tx_rx_loopback()
